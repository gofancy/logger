package wtf.gofancy.logger;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

import java.util.function.Consumer;

public class L {

    private final Logger logger;
    private final Marker marker;

    public L(final String name, final String marker) {
        this(LoggerFactory.getLogger(name), marker);
    }

    public L(final Logger logger, final String marker) {
        this.logger = logger;
        this.marker = MarkerFactory.getMarker(marker);
    }

    // ERROR >>
    public void e(final String msg) {
        this.logger.error(this.marker, msg);
    }

    public void e(final String format, final Object arg) {
        this.logger.error(this.marker, format, arg);
    }

    public void e(final String format, final Object arg1, final Object arg2) {
        this.logger.error(this.marker, format, arg1, arg2);
    }

    public void e(final String format, final Object... args) {
        this.logger.error(this.marker, format, args);
    }

    public void e(final String msg, final Throwable t) {
        this.logger.error(this.marker, msg, t);
    }

    public void eb(final String msg) {
        this.eb(DumpStackBehavior.CLOSEST_DUMP, msg);
    }

    public void eb(final DumpStackBehavior dsb, final String msg) {
        Utils.block(msg, dsb, this::e);
    }

    public void eb(final Consumer<StringBuilder> blockBuilder) {
        final StringBuilder builder = new StringBuilder();
        blockBuilder.accept(builder);
        this.eb(builder.toString());
    }

    public void eb(final DumpStackBehavior dsb, final Consumer<StringBuilder> blockBuilder) {
        final StringBuilder builder = new StringBuilder();
        blockBuilder.accept(builder);
        this.eb(dsb, builder.toString());
    }
    // << ERROR

    // WARN >>
    public void w(final String msg) {
        this.logger.warn(this.marker, msg);
    }

    public void w(final String format, final Object arg) {
        this.logger.warn(this.marker, format, arg);
    }

    public void w(final String format, final Object arg1, final Object arg2) {
        this.logger.warn(this.marker, format, arg1, arg2);
    }

    public void w(final String format, final Object... args) {
        this.logger.warn(this.marker, format, args);
    }

    public void w(final String msg, final Throwable t) {
        this.logger.warn(this.marker, msg, t);
    }

    public void wb(final String msg) {
        this.wb(DumpStackBehavior.CLOSEST_DUMP, msg);
    }

    public void wb(final DumpStackBehavior dsb, final String msg) {
        Utils.block(msg, dsb, this::w);
    }

    public void wb(final Consumer<StringBuilder> blockBuilder) {
        final StringBuilder builder = new StringBuilder();
        blockBuilder.accept(builder);
        this.wb(builder.toString());
    }

    public void wb(final DumpStackBehavior dsb, final Consumer<StringBuilder> blockBuilder) {
        final StringBuilder builder = new StringBuilder();
        blockBuilder.accept(builder);
        this.wb(dsb, builder.toString());
    }
    // << WARN

    // INFO >>
    public void i(final String msg) {
        this.logger.info(this.marker, msg);
    }

    public void i(final String format, final Object arg) {
        this.logger.info(this.marker, format, arg);
    }

    public void i(final String format, final Object arg1, final Object arg2) {
        this.logger.info(this.marker, format, arg1, arg2);
    }

    public void i(final String format, final Object... args) {
        this.logger.info(this.marker, format, args);
    }

    public void i(final String msg, final Throwable t) {
        this.logger.info(this.marker, msg, t);
    }

    public void ib(final String msg) {
        this.ib(DumpStackBehavior.CLOSEST_DUMP, msg);
    }

    public void ib(final DumpStackBehavior dsb, final String msg) {
        Utils.block(msg, dsb, this::i);
    }

    public void ib(final Consumer<StringBuilder> blockBuilder) {
        final StringBuilder builder = new StringBuilder();
        blockBuilder.accept(builder);
        this.ib(builder.toString());
    }

    public void ib(final DumpStackBehavior dsb, final Consumer<StringBuilder> blockBuilder) {
        final StringBuilder builder = new StringBuilder();
        blockBuilder.accept(builder);
        this.ib(dsb, builder.toString());
    }
    // << INFO

    // DEBUG >>
    public void d(final String msg) {
        this.logger.debug(this.marker, msg);
    }

    public void d(final String format, final Object arg) {
        this.logger.debug(this.marker, format, arg);
    }

    public void d(final String format, final Object arg1, final Object arg2) {
        this.logger.debug(this.marker, format, arg1, arg2);
    }

    public void d(final String format, final Object... args) {
        this.logger.debug(this.marker, format, args);
    }

    public void d(final String msg, final Throwable t) {
        this.logger.debug(this.marker, msg, t);
    }

    public void db(final String msg) {
        this.db(DumpStackBehavior.CLOSEST_DUMP, msg);
    }

    public void db(final DumpStackBehavior dsb, final String msg) {
        Utils.block(msg, dsb, this::d);
    }

    public void db(final Consumer<StringBuilder> blockBuilder) {
        final StringBuilder builder = new StringBuilder();
        blockBuilder.accept(builder);
        this.db(builder.toString());
    }

    public void db(final DumpStackBehavior dsb, final Consumer<StringBuilder> blockBuilder) {
        final StringBuilder builder = new StringBuilder();
        blockBuilder.accept(builder);
        this.db(dsb, builder.toString());
    }
    // << DEBUG

    // TRACE >>
    public void t(final String msg) {
        this.logger.trace(this.marker, msg);
    }

    public void t(final String format, final Object arg) {
        this.logger.trace(this.marker, format, arg);
    }

    public void t(final String format, final Object arg1, final Object arg2) {
        this.logger.trace(this.marker, format, arg1, arg2);
    }

    public void t(final String format, final Object... args) {
        this.logger.trace(this.marker, format, args);
    }

    public void t(final String msg, final Throwable t) {
        this.logger.trace(this.marker, msg, t);
    }

    public void tb(final String msg) {
        this.tb(DumpStackBehavior.CLOSEST_DUMP, msg);
    }

    public void tb(final DumpStackBehavior dsb, final String msg) {
        Utils.block(msg, dsb, this::t);
    }

    public void tb(final Consumer<StringBuilder> blockBuilder) {
        final StringBuilder builder = new StringBuilder();
        blockBuilder.accept(builder);
        this.tb(builder.toString());
    }

    public void tb(final DumpStackBehavior dsb, final Consumer<StringBuilder> blockBuilder) {
        final StringBuilder builder = new StringBuilder();
        blockBuilder.accept(builder);
        this.tb(dsb, builder.toString());
    }
    // << TRACE
}
