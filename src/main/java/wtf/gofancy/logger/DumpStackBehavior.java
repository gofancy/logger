package wtf.gofancy.logger;

public enum DumpStackBehavior {
    /**
     * no stack trace
     */
    DO_NOT_DUMP,
    /**
     * starting at logging call up to 6 lines of stack trace
     */
    CLOSEST_DUMP,
    /**
     * starting at logging call print all lines of stack trace
     */
    FULL_DUMP,
    /**
     * complete stack trace (includes logging internal calls)
     */
    COMPLETE_DUMP
}
