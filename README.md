# Garden of Fancy Logger

An abstraction layer over [SLF4J](http://www.slf4j.org/) with one class which provides the logging methods.

It is not needed to add SLF4J as extra dependency. A [binding](http://www.slf4j.org/manual.html#swapping) is still
needed to see any actual logging happen.

# Legacy Library, use [FancyLog](https://gitlab.com/gofancy/fancylog) instead
